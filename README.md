# Login-Register-JWT-App

## Project setup
```
cd login-register-jwt-app
./install.sh
```

### Start server
```
cd server/ && yarn start:testing
```

### Start Vue app
```
# New tab
cd client/ && yarn serve:testing
```

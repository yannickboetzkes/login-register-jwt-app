import Vue from "vue";
import Router, { Route } from "vue-router";
import Home from "./views/Home.vue";
import { userStore } from "@/store/user-store";

Vue.use(Router);

export default new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes: [
		{
			path: "/",
			name: "home",
			component: Home,
		},
		{
			path: "/dashboard",
			name: "dashboard",
			// route level code-splitting; configuring webpack is necessary (-> config.vue.js)
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "dashboard-chunk" */ "./views/Dashboard.vue"),
			/* -------- Route Guard/Auth Guard -------- */
			/*async*/ beforeEnter(routeTo, routeFrom, next) {
				if (userStore.loggedIn) {
					next();
				} else {
					next({ name: "login" });
				}
			},
		},
		{
			path: "/register",
			name: "register",
			component: () => import(/* webpackChunkName: "register-chunk" */ "./views/RegisterUser.vue"),
		},
		{
			path: "/login",
			name: "login",
			component: () => import(/* webpackChunkName: "login-chunk" */ "./views/LoginUser.vue"),
		},
	],
});

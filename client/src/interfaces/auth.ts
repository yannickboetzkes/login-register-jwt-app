export interface IRegisterDTO {
	name: string;
	email: string;
	password: string;
}

export interface ILoginDTO {
	email: string;
	password: string;
}

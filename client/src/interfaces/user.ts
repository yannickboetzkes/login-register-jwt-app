// export interface IUserState {
// 	user: IUser
// }

export interface IUser {
	name: string;
	email: string;
	password: string;
}

export interface IAuthenticatedUser extends Document {
	name: string;
	email: string;
	token: string;
	expirationTime: string;
	tokenExpirationDate?: Date;
}

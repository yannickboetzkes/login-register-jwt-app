import "./hooks";

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import { errorStore } from "@/store/error-store";

Vue.config.productionTip = false;

/* -------- GLOBAL ERROR HANDLER -------- */
// Here errors are exceptions that are thrown in JavaScript and not handled (e.g. no catch where error is thrown)
Vue.config.errorHandler = function(err: any, vm, info) {
	// console.log(err.response);
	errorStore.SET_ERROR(err.response.data.message);
	// if (process.env.NODE_ENV != "development") {}
};

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount("#app");

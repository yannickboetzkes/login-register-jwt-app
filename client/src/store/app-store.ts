import { getModule, Module, Mutation, VuexModule } from "vuex-module-decorators";
import store from "@/store/store";

@Module({ namespaced: true, dynamic: true, store, name: "appStore" })
export class AppStore extends VuexModule {
	/* -------- STATE --------
	 *
	 */
	isLoading: boolean = false;

	/* -------- GETTERS --------
	 *
	 */
	get getIsLoading() {
		return this.isLoading;
	}

	/* -------- MUTATIONS --------
	 *
	 */
	@Mutation
	SET_LOADING_STATUS(status: boolean) {
		this.isLoading = status;
	}

	/* -------- ACTIONS --------
	 *
	 */
}

export const appStore = getModule(AppStore);

import { Action, getModule, Module, Mutation, MutationAction, VuexModule } from "vuex-module-decorators";
import { authService } from "@/services/auth-service";
import store from "@/store/store";
import { IAuthenticatedUser } from "@/interfaces/user";
import { ILoginDTO, IRegisterDTO } from "@/interfaces/auth";
import router from "@/router";
import { errorStore } from "@/store/error-store";

@Module({ namespaced: true, dynamic: true, store, name: "userStore" })
export class UserStore extends VuexModule {
	/* -------- STATE --------
	 *
	 */
	user: IAuthenticatedUser | null = null;

	/* -------- GETTERS --------
	 *
	 */
	get loggedIn() {
		return !!this.user; // returns true or false
	}

	/* -------- MUTATIONS --------
	 *
	 */
	@Mutation
	SET_USER_DATA(userData: IAuthenticatedUser) {
		console.log("USER DATA MUTATION", userData);
		if (!userData.tokenExpirationDate) {
			const now = new Date();
			const tokenExpirationDate = new Date(now.getTime() + +userData.expirationTime * 1000);
			userData.tokenExpirationDate = tokenExpirationDate;
		}
		this.user = userData;
		localStorage.setItem("user", JSON.stringify(userData));
		authService.setTokenHeader(userData.token);
	}

	@Mutation
	CLEAR_USER_DATA() {
		this.user = null;
		localStorage.removeItem("user");
		authService.deleteTokenHeader();
	}

	/* -------- ACTIONS --------
	 *
	 */
	@Action({ rawError: true })
	async registerUser(registerDTO: IRegisterDTO) {
		const user = await authService.register(registerDTO);
		this.context.commit("SET_USER_DATA", user);
	}

	@Action({ rawError: true })
	async loginUser(loginDTO: ILoginDTO) {
		try {
			const user = await authService.login(loginDTO);
			this.context.commit("SET_USER_DATA", user);
			this.setLogoutTimer(user.expirationTime);
		} catch (err) {
			errorStore.SET_ERROR(err.response.data.message);
		}
	}

	@Action
	logout() {
		this.context.commit("CLEAR_USER_DATA");
		router.replace({ name: "login" });
	}

	@Action({ rawError: true })
	tryAutoLogin() {
		const user = JSON.parse(localStorage.getItem("user")!) || {};

		if (!user.token) {
			return;
		}
		const now = new Date();
		const expirationDate = new Date(user.tokenExpirationDate);
		if (now >= expirationDate) {
			this.context.commit("CLEAR_USER_DATA");
			return;
		}
		this.context.commit("SET_USER_DATA", user);
		router.push({ name: "dashboard" });
	}

	@Action
	setLogoutTimer(tokenExpirationTime) {
		setTimeout(() => {
			this.logout();
		}, tokenExpirationTime * 1000);
	}
}

export const userStore = getModule(UserStore);

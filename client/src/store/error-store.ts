import { getModule, Module, Mutation, VuexModule } from "vuex-module-decorators";
import store from "@/store/store";

@Module({ namespaced: true, dynamic: true, store, name: "errorStore" })
export class ErrorStore extends VuexModule {
	/* -------- STATE --------
	 *
	 */
	errorActive: boolean = false;
	errorMessage: string = "";

	/* -------- GETTERS --------
	 *
	 */
	get getErrorData() {
		return {
			message: this.errorMessage,
			active: this.errorActive,
		};
	}

	/* -------- MUTATIONS --------
	 *
	 */
	@Mutation
	SET_ERROR(errorMessage, errorTime = 3000) {
		this.errorMessage = errorMessage;
		this.errorActive = true;
		setTimeout(() => {
			this.errorActive = false;
		}, errorTime);
	}

	/* -------- ACTIONS --------
	 *
	 */
}
export const errorStore = getModule(ErrorStore);

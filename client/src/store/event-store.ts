import { Action, getModule, Module, Mutation, VuexModule } from "vuex-module-decorators";
import store from "@/store/store";
import { IEvent } from "@/interfaces/event";
import { eventService } from "@/services/event-service";

@Module({ namespaced: true, dynamic: true, store, name: "eventStore" })
export class EventStore extends VuexModule {
	/* -------- STATE --------
	 *
	 */
	events: IEvent[] | null = null;
	/* -------- GETTERS --------
	 *
	 */
	get getEvents() {
		return this.events;
	}

	/* -------- MUTATIONS --------
	 *
	 */
	@Mutation
	SET_EVENTS(events: IEvent[]) {
		this.events = events;
	}

	/* -------- ACTIONS --------
	 *
	 */
	@Action({ rawError: true })
	async fetchEvents() {
		// TODO: create helper function that uses this in function body without passing this
		// this.context.commit("appStore/SET_LOADING_STATUS", true, { root: true });
		setLoadingStatus(this, true);
		const events = await eventService.getEvents();
		this.context.commit("SET_EVENTS", events);
		setLoadingStatus(this, false);
	}
}
export const eventStore = getModule(EventStore);

const setLoadingStatus = (instance, status) => {
	instance.context.commit("appStore/SET_LOADING_STATUS", status, { root: true });
};

import { apiService } from "@/services/api-service";
import { ILoginDTO, IRegisterDTO } from "@/interfaces/auth";
import { IAuthenticatedUser } from "@/interfaces/user";

class AuthService {
	async register(registerDTO: IRegisterDTO): Promise<IAuthenticatedUser> {
		return await apiService.post("auth/register", registerDTO);
	}

	async login(loginDTO: ILoginDTO): Promise<IAuthenticatedUser> {
		return await apiService.post("auth/login", loginDTO);
	}

	/* -------- HELPERS -------- */
	setTokenHeader(token) {
		apiService.setHeaders("Authorization", `Bearer ${token}`);
	}

	deleteTokenHeader() {
		apiService.deleteHeader("Authorization");
	}
}

export const authService = new AuthService();

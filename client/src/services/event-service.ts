import { apiService } from "@/services/api-service";

class EventService {
	getEvents(): Promise<any> {
		return apiService.get("event");
	}
}

export const eventService = new EventService();

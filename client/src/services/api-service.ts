import axios from "axios";
// import { response } from "express";

const apiClient = axios.create({
	baseURL: process.env.VUE_APP_API_URL,
	headers: {
		Accept: "application/json",
		"Content-Type": "application/json",
	},
});

class ApiService {
	async get(endpoint: string): Promise<any> {
		const response = await apiClient.get(endpoint);
		if (process.env.NODE_ENV == "development") {
			console.log("[API SERVICE LOGGER]", response);
			console.log("[API SERVICE LOGGER; -- DATA --]", response.data);
		}
		return response.data;
	}

	async post(endpoint: string, payload: any): Promise<any> {
		const response = await apiClient.post(endpoint, payload);
		if (process.env.NODE_ENV == "development") {
			console.log("[API SERVICE LOGGER]", response);
			console.log("[API SERVICE LOGGER; -- DATA --]", response.data);
		}
		return response.data;
	}

	/* -------- HELPERS -------- */
	setHeaders(header: string, value: string) {
		apiClient.defaults.headers.common[header] = value;
	}

	deleteHeader(header: string) {
		delete apiClient.defaults.headers.common[header];
	}
}

export const apiService = new ApiService();

/* -------- INTERCEPTOR -------- */
// apiClient.interceptors.response.use(
// 	response => {
// 		console.log("testing");
// 		return response;
// 	},
// 	err => {
// 		if (process.env.NODE_ENV == "development") {
// 			console.log(err.response);
// 		}
// 		return Promise.reject(err); // or throw new Error("An error occurred");
// 	},
// );

import Component from "vue-class-component";
// additional hooks other than create() have to be setup in hooks.ts

Component.registerHooks(["beforeRouteEnter", "beforeRouteUpdate", "beforeRouteLeave"]);

// vue.config.js
module.exports = {
	// ... your other options
	transpileDependencies: [
		'vuex-module-decorators'
	],
	chainWebpack: config => {
		/* -------- remove the prefetch plugin -------- */
		// config.plugins.delete('prefetch')
		// or:
		// modify its options:
		config.plugin('prefetch').tap(options => {
			options[0].fileBlacklist = options[0].fileBlacklist || []
			options[0].fileBlacklist.push(/.+-chunk(.*)+?\.js$/)
			return options
		})
	}
	
};

import { Schema, Document, HookNextFunction } from "mongoose";
import * as bcrypt from "bcrypt";

export const UserSchema = new Schema({
	name: {
		type: String,
		trim: true, // trim whitespace
		required: true,
	},
	password: {
		type: String,
		trim: true,
		select: false, // not returned when user is fetched
	},
	email: {
		type: String,
		trim: true, // trim whitespace
		required: true,
		validate: {
			// on save() run validation: check if user already exists
			validator: async function(val) {
				const user = await this.constructor.findOne({ email: val });
				return !user;
			},
			// message: "Email already exists",
		},
	},
});

export interface IUser extends Document {
	name: string;
	email: string;
	password: string;
	getJwtPayload: IJwtPayload;
	getSanitizedUser: IUserSanitized;
	comparePassword: (sentPassword: string) => boolean;
}

export interface IUserSanitized extends Document {
	name: string;
	email: string;
	getJwtPayload: IJwtPayload;
	getSanitizedUser: IUserSanitized;
	comparePassword: (sentPassword: string) => boolean;
}

export interface IAuthenticatedUser extends Document {
	name: string;
	email: string;
	token: string;
	expirationTime: string;
}

export interface IJwtPayload {
	name: string;
	email: string;
}

/* -------- HOOKS --------
 *
 */
UserSchema.pre("save", async function(next: HookNextFunction) {
	try {
		// only hash the password if it has been modified (or is new)
		if (!this.isModified("password")) {
			return next();
		}
		const hashed = await bcrypt.hash(this["password"], 10);
		this["password"] = hashed;

		return next();
	} catch (err) {
		return next(err);
	}
});

// not writing to db; modifies returned response
UserSchema.post("save", function(doc: Document, next: HookNextFunction) {
	this["password"] = null;
	return next();
});

/* --------  VIRTUALS  --------
 *
 */
UserSchema.virtual("getJwtPayload").get(function() {
	return {
		name: this.name,
		email: this.email,
	};
});

UserSchema.virtual("getSanitizedUser").get(function() {
	this["password"] = null;
	return this;
});

/* -------- METHODS --------
 *
 */
UserSchema.methods.comparePassword = function(sentPassword) {
	console.log("sent password:", sentPassword, "| Db password:", this.password);
	return bcrypt.compare(sentPassword, this.password);
};

import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IJwtPayload, IUser, IUserSanitized } from "./user.schema";
import { ILoginDTO, IRegisterDTO } from "../auth/auth.dto";

@Injectable()
export class UserService {
	constructor(@InjectModel("User") private userModel: Model<IUser>) {}

	async create(userDTO: IRegisterDTO): Promise<IUser> {
		try {
			const newUser = await new this.userModel(userDTO).save();
			console.log("[USER SERVICE]", newUser);
			return newUser;
		} catch (err) {
			throw new HttpException("User konnte nicht erstellt werden.", HttpStatus.BAD_REQUEST);
		}
	}

	async findUserByEmailAndName(jwtPayload: IJwtPayload): Promise<IUser> {
		const { email, name } = jwtPayload;
		try {
			const user = this.userModel.findOne({ email, name });
			return user;
		} catch (err) {
			throw new HttpException("User konnte nicht gefunden werden.", HttpStatus.BAD_REQUEST);
		}
	}

	async findUserByEmailAndPassword(loginDTO: ILoginDTO): Promise<IUserSanitized> {
		const { email, password } = loginDTO;
		try {
			const user = await this.userModel.findOne({ email }).select("+password");
			const validPassword = await user.comparePassword(password);
			// prettier-ignore
			return validPassword ? user.getSanitizedUser : (function() { throw Error })();
		} catch (err) {
			throw new HttpException("Deine Email oder deine Passwort ist nicht korrekt.", HttpStatus.UNAUTHORIZED);
		}
	}
}

import { ArgumentsHost, Catch, ExceptionFilter, HttpException, Res, Logger, HttpStatus } from "@nestjs/common";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
	catch(exception: HttpException, host: ArgumentsHost) {
		const context = host.switchToHttp();
		const response: any = context.getResponse<Response>();
		const request = context.getRequest<Request>();
		const status = exception.getStatus() ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR; // HttpStatus.INTERNAL_SERVER_ERROR evaluates to a number

		const errorResponseObject: any = {
			code: status,
			timestamp: new Date().toLocaleDateString(),
			path: request.url,
			method: request.method,
			// response can be passed in HttpException(%response%, %status%)
			message: exception.getResponse()["message"] || exception.getResponse(),
		};
		// readable syntax
		if (exception.getResponse()["err"] && process.env.NODE_ENV == "dev") {
			errorResponseObject.error = exception.getResponse()["err"];
		}

		Logger.error(
			`Called url: ${request.method} ${request.url}`,
			`[ErrorFilter] Error: ${JSON.stringify(errorResponseObject, undefined, 2)}`,
			"ExceptionFilter",
		);

		return response.status(status).json(errorResponseObject);
	}
}

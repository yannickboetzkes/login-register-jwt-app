import { Schema, Document } from "mongoose";

export const EventSchema = new Schema(
	{
		eventId: {
			type: String,
		},
		title: {
			type: String,
		},
		time: {
			type: String,
		},
		date: {
			type: String,
		},
	},
	{ timestamps: true },
);

export interface IEvent extends Document {
	eventId: string;
	title: string;
	time: string;
	date: string;
}

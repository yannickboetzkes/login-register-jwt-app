export interface IEventDTO {
	id: string;
	title: string;
	time: string;
	date: string;
}

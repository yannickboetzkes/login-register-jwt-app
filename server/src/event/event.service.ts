import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IEvent } from "./event.schema";
import { IEventDTO } from "./event.dto";

@Injectable()
export class EventService {
	constructor(@InjectModel("Event") private eventModel: Model<IEvent>) {}

	async getAll(): Promise<IEvent[]> {
		try {
			return await this.eventModel.find();
		} catch (err) {
			throw new HttpException("Events konnten nicht geladen werden.", HttpStatus.BAD_REQUEST);
		}
	}

	async create(eventDTO: IEventDTO): Promise<IEvent> {
		try {
			return await new this.eventModel(eventDTO).save();
		} catch (err) {
			throw new HttpException("Event konnte nicht erstellt werden.", HttpStatus.BAD_REQUEST);
		}
	}
}

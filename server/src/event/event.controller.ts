import { Body, Controller, Get, Post, UseGuards } from "@nestjs/common";
import { EventService } from "./event.service";
import { IEventDTO } from "./event.dto";
import { AuthGuard } from "@nestjs/passport";
import { JwtAuthGuard } from "../auth/passport/jwt.strategy.service";

@Controller("event")
export class EventController {
	constructor(private eventService: EventService) {}

	@Get()
	@UseGuards(JwtAuthGuard)
	async listAllEvents() {
		return await this.eventService.getAll();
	}

	@Post()
	async createEvent(@Body() event: IEventDTO) {
		return await this.eventService.create(event);
	}
}

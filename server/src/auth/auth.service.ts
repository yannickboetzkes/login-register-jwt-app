import { Injectable } from "@nestjs/common";
import { UserService } from "../user/user.service";
import { IJwtPayload, IUser, IUserSanitized } from "../user/user.schema";
import * as jwt from "jsonwebtoken";

@Injectable()
export class AuthService {
	constructor(private userService: UserService) {}

	createToken(user: IUserSanitized) {
		const userJwtP: IJwtPayload = user.getJwtPayload;
		console.log(process.env.TOKEN_EXPIRATION_TIME);
		const token = jwt.sign(userJwtP, process.env.JWT_SECRET, { expiresIn: +process.env.TOKEN_EXPIRATION_TIME });
		return {
			token,
			expirationTime: +process.env.TOKEN_EXPIRATION_TIME,
		};
	}

	async validateUser(jwtPayload: IJwtPayload) {
		return await this.userService.findUserByEmailAndName(jwtPayload);
	}
}

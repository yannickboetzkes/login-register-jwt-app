import { Body, Controller, Post } from "@nestjs/common";
import { IRegisterDTO, ILoginDTO } from "./auth.dto";
import { UserService } from "../user/user.service";
import { AuthService } from "./auth.service";
import { IAuthenticatedUser } from "../user/user.schema";

@Controller("auth")
export class AuthController {
	constructor(private userService: UserService, private authService: AuthService) {}

	@Post("register")
	async createUser(@Body() registerDTO: IRegisterDTO): Promise<IAuthenticatedUser> {
		const user = await this.userService.create(registerDTO);
		const token = await this.authService.createToken(user);
		return { ...user.toObject(), ...token };
	}

	@Post("login")
	async loginUser(@Body() loginDTO: ILoginDTO): Promise<IAuthenticatedUser> {
		const user = await this.userService.findUserByEmailAndPassword(loginDTO);
		const token = await this.authService.createToken(user);
		return { ...user.toObject(), ...token };
	}
}

import { ExecutionContext, Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { AuthGuard, PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy, VerifiedCallback } from "passport-jwt";
import { IJwtPayload } from "../../user/user.schema";
import { AuthService } from "../auth.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(private authService: AuthService) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			secretOrKey: process.env.JWT_SECRET,
		});
	}

	async validate(payload: IJwtPayload, done: VerifiedCallback) {
		console.log("---- Token verified; fetching user ----", payload);
		const user = await this.authService.validateUser(payload);
		done(null, user);
	}
}

/* -------- CUSTOM ERROR HANDLING (on fail extracting and verifying token -------- */
@Injectable()
export class JwtAuthGuard extends AuthGuard("jwt") {
	canActivate(context: ExecutionContext) {
		return super.canActivate(context);
	}

	handleRequest(err, user, info) {
		if (err || !user) {
			// custom error handling
			Logger.error(info.message, null, info.name);
			throw err || new UnauthorizedException();
		}
		return user;
	}
}

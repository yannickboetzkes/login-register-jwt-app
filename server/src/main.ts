const args = require("minimist")(process.argv.slice(2));
const mode = args["mode"];

// Todo: adjust for different server environments
if (mode === "testing") {
	require("dotenv").config({ path: "./.env.testing" });
} else if (mode === "staging") {
	require("dotenv").config({ path: "./.env.production" });
} else {
	require("dotenv/config");
}

import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.setGlobalPrefix("api");
	app.enableCors();
	await app.listen(process.env.PORT);
}
bootstrap();

import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { EventModule } from "./event/event.module";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthModule } from "./auth/auth.module";
import { UserModule } from "./user/user.module";
import { APP_FILTER } from "@nestjs/core";
import { HttpExceptionFilter } from "./shared/http-exception-filter";

@Module({
	imports: [
		MongooseModule.forRoot(process.env.MONGO_URI, {
			useNewUrlParser: true,
			useFindAndModify: false,
		}),
		EventModule,
		AuthModule,
		UserModule,
	],
	controllers: [AppController],
	providers: [
		AppService,
		{
			provide: APP_FILTER,
			useClass: HttpExceptionFilter,
		},
	],
})
export class AppModule {}
